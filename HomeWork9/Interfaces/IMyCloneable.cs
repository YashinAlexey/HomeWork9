﻿
namespace HomeWork9.Interfaces
{
    public interface IMyCloneable<T> where T: class
    {
        public T MyClone();
    }
}
