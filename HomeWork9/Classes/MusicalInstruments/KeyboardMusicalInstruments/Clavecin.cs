﻿using HomeWork9.Interfaces;
using System;

namespace HomeWork9.Classes.MusicalInstruments.KeyboardMusicalInstruments
{
    public class Clavecin : KeyboardMusicalInstrument, IMyCloneable<Clavecin>
    {
        public string Manufacturer { get; set; }

        public Clavecin(string manufacturer)
            => Manufacturer = manufacturer;

        public Clavecin(Clavecin source) : base(source)
            => Manufacturer = source.Manufacturer;

        public new Clavecin MyClone()
            => new Clavecin(this);

        public new object Clone()
            => MyClone();

        public override string ToString()
        {
            return $"Production year: {ProductionYear}; Number of keys: {NumberOfKeys}; Manufacturer: {Manufacturer}";
        }
    }
}
