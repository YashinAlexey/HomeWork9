﻿using HomeWork9.Interfaces;
using System;

namespace HomeWork9.Classes.MusicalInstruments.KeyboardMusicalInstruments
{
    public class KeyboardMusicalInstrument : MusicalInstrument, IMyCloneable<KeyboardMusicalInstrument>
    {
        public ushort NumberOfKeys { get; set; }

        public KeyboardMusicalInstrument() { }

        public KeyboardMusicalInstrument(ushort numberOfKeys)
            => NumberOfKeys = numberOfKeys;

        public KeyboardMusicalInstrument(KeyboardMusicalInstrument source) : base(source)
            => NumberOfKeys = source.NumberOfKeys;

        public new KeyboardMusicalInstrument MyClone()
            => new KeyboardMusicalInstrument(this);

        public new object Clone()
            => MyClone();
    }
}
