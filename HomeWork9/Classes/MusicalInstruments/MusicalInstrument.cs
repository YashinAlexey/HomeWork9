﻿using HomeWork9.Interfaces;
using System;

namespace HomeWork9.Classes.MusicalInstruments
{
    public class MusicalInstrument : IMyCloneable<MusicalInstrument>, ICloneable
    {
        public ushort ProductionYear { get; set; }

        public MusicalInstrument() { }           

        public MusicalInstrument(ushort productionYear)
            => ProductionYear = productionYear;

        public MusicalInstrument(MusicalInstrument source)
            => ProductionYear = source.ProductionYear;

        public MusicalInstrument MyClone()
            => new MusicalInstrument(this);

        public object Clone()
            => MyClone();       
    }
}
