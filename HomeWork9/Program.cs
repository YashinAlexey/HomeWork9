﻿using System;
using HomeWork9.Classes.MusicalInstruments.KeyboardMusicalInstruments;

namespace HomeWork9
{
    class Program
    {
        static void Main(string[] args)
        {
            Clavecin clavecin = new Clavecin("Rückers");
            clavecin.NumberOfKeys = 60;
            clavecin.ProductionYear = 1594;

            Console.WriteLine(clavecin);

            Clavecin otherClavecin1 = clavecin.MyClone();
            otherClavecin1.ProductionYear = 1614;

            Console.WriteLine(otherClavecin1);            

            Clavecin otherClavecin2 = (Clavecin)otherClavecin1.Clone();
            otherClavecin2.Manufacturer = "Neupert";

            Console.WriteLine(otherClavecin2);

            Console.ReadLine();
        }
    }
}
